# Wiki

This repository contains a collection of conventions for OpenSavvy projects. 
We encourage you to use them in your own projects, too.

Most of the time, we attempt to keep our conventions as close as possible to what is idiomatic—for example, our Kotlin coding style is almost identical to the official one written by JetBrains, the only differences are additions to things that weren't mentioned, or decisions when the official style gives a choice.

## Tutorials

Step-by-step guides to achieve a specific objective.

**Contributing**
- Reporting a problem, making a feature request
- Contributing to a repository
  - [Initial setup: forks and why we use them](tutorials/forks.md)
  - Making changes and submitting them to the upstream repository
  - Acting on comments during code review

**Tools**
- Setting up your own GitLab CI runner

## Conventions

Detailed explanations of concepts and guidelines we adhere to.

**Code style**
- [Baseline](coding-style/README.md)
- [Kotlin-specific](coding-style/kotlin.md)

**Tools**
- [Git](tooling/git/README.md)
- [GitLab](tooling/gitlab/README.md)

**Other**
- [Licensing](licensing/README.md)

## Using in your projects

We recommend to add a "Contributing" section in your README, or in a CONTRIBUTING file, which links to this repository and states which parts of our conventions you are following.

## License

This project is licensed under Apache 2.0, see the full text in the [LICENSE.txt file](LICENSE.txt).
