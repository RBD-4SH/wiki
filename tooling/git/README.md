# Git conventions

We use Git to manage our versions.

- [Commit style](commits.md)
- [Workflow](flow.md)
