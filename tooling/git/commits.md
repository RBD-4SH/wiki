# OpenSavvy Git commit style

This file explains the choices we have made in our commit style.

A commit should always look like this:

```text
Header

Body

Footer
```

The rationale behind all of these choices is to keep commits small and easy to understand. The main benefits are:

- Maintaining the code becomes simpler, as it is easy to know what changed what.
- Help users of your code find important information quickly.
- Help with history modification (rebase, cherry-pick,...).
- Changelogs can be automated.
- Semantic versioning can be automated.

## Header

The header is mandatory for all commits.

It should be composed of the following parts:

- the type of the changes
- the affected scope
- the description of the commit

These should be arranged in the following manner:
`type(scope): description`

In some cases it is allowed to omit the scope. In those cases, use the following syntax: `type: description` or `type(): description`

### Types

- Modifications to the build environment: `build` or `Build`
- Upgrade of the dependencies: `upgrade`, `Upgrade`
- Modifications to the CI/CD environment: `ci`
- Modifications to the documentation: `doc`, `docs` or `Docs`
- New features that are backward-compatible: `feat` or `New`
- Fixing problems, backward-compatible: `fix`, `Fix`, `fixes`
- Performance improvements: `perf`, `perfs` or `Update`
- Refactoring, internal changes to the implementation: `refactor` or `Update`
- Grammar, coding style, linting...: `style`
- Work on tests: `test` or `tests`
- Breaking changes, backward-incompatible: `breaking` or `Breaking`
- In Git flow and similar, the merge commits can have the type `merge`. Note that merge commits are not recommended otherwise.

We chose these types so users accustomed to the Angular or ESLint commit styles do not have to change their habits.

In the absence of personal preference, the first version listed for each type should be preferred (e.g. prefer the
lowercase version).

If you cannot find a single type that corresponds to your changes, consider splitting your commit into smaller ones.

### Scope

The scope should always be lowercase. Prefer a single word. If it's not possible to use a single word, `lisp-case` is
tolerated.

If you are modifying the configuration of a tool (build tools like Make and Gradle, dependency management tools like
NPM, environments like Docker, etc), the scope should probably be its name.

Otherwise, the scope should be a simple identifier to help the user know which part of the project is changed, in a
general sense. The scope should NOT be a filename. As a rule of thumb, the following definitions are probably good:

- For Java projects, or other languages with a concept of packages, the more precise package name is accurate. For
  example, if the project contains `fr.braindot.main` and `fr.braindot.utils`, `main` and `utils` are valid scopes.
- For modular projects, the name of the module is a good name. For example, if your project has a `server` and
  `client` modules, these are valid names.

If your changes affect multiple scopes, consider splitting your commit into smaller ones: it is forbidden to put
multiple scopes.
If your changes affect the project as a whole, you can omit the scope.
If the project is too small for scopes to make sense, it is fine to omit it.

Keep in mind that scopes are here to help find what your changes are about.

### Description

The description should be written in the past tense. Explain the reason behind your changes, not what you did: anyone
can read the diff to know what has changed.

For example, if you fixed a problem, explain what the problem was, not how you fixed it. If you have to explain what
you did, because it's not clear enough, put that in the commit's body.

## Body

The body is optional in most cases. If the body is long enough that it needs to be structured, it should follow the
MarkDown syntax.

If you feel like your changes are not easy to understand, you should add a body that explains what the complicated
parts are, and why you decided to do those.

For breaking changes, always include a body that explains what the change is, and if needed the reason why it was
necessary.

In merge-based workflows (Git flow, GitHub flow, GitLab flow...), the merge commit should have a body that explains
what the branch as a whole adds, and what the consequences are.
If you are creating a merge request or pull request, the title should be the commit's header, and the description
should be the commit's body and footer.
This allows the reviewer to easily merge (for example, GitLab has a 'include merge request description' option).

## Footer

The footer is optional. It is used to specify commands for the different tools used. This can be used to
automatically close an issue (for example `Fixes #12` with GitLab) or to do quick actions. We do not
define a syntax in particular, use whatever is standard for the tools you use.

You can also add trailers to add more information about the commit (trailers should always be the last lines of the commit, at the end of the footer):

- Use `Co-authored-by: Full Name <email>` to give the identity of anyone who helped you write the commit (useful for pair programming).
- When committing someone else's work, use `Signed-off-by: Full Name <email>` to give the identity of each person responsible for allowing the contribution (and leave the author field to its original value). This is useful when upstreaming improvements made in a closed source repository: add each person who approved the public release.

## Automation

Regular expression to verify commit headers; can be used in GitLab and JetBrains products.

```regexp
(?:build|Build|upgrade|Upgrade|ci|doc|docs|Docs|feat|New|fix|fixes|Fix|perf|perfs|Update|refactor|style|test|tests|breaking|Breaking|merge)(?:\([a-z-]*\))?: .+
```
