# GitLab conventions

We recommend using GitLab to host projects' sources and documentation (whether they are code or any other format).

- [Configuration for new projects](setup.md)
