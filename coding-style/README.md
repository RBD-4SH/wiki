# OpenSavvy coding style

This directory contains the description of the OpenSavvy coding style and its variants for the various programming languages we use.
Generally, the coding style for each language is a mix of our general coding style and of the official coding style for that language.

The goal is for adoption to be easy, while still providing a set of common rules, to ensure familiarity between the
codebases, even if they are in different languages.

Each language may have additional rules, which are available in separate files:

- [Kotlin](kotlin.md)

> For IntelliJ-based IDEs, the code style is available in this repository's [.idea directory](../.idea/codeStyles/Project.xml).
> Go to "Settings → Editor → Code style", click the cog next to "Scheme" at the top of the page, select "Import Scheme → IntelliJ IDEA code style XML", then select the file linked above.

The rest of this document describes rules that are common to all languages.

## Text formatting

This section describes rules for generic text.

### Trailing spaces

Lines should not end with trailing whitespace.

> In IntelliJ-based IDEs, configure this in "Settings → Editor → General", "On save". Tick "Remove line breaks on
> modified lines".

### End of files

All files should end with a proper line terminating character.
Improperly terminated files make diffs harder to read, and cause display problems in terminals.

Generated files which are not meant to be edited by humans are exempt from this rule.

> In IntelliJ-based IDEs, configure this in "Settings → Editor → General", "On save". Tick "Remove trailing blank lines
> at the end of saved files" and "Ensure every saved file ends with a line break".

### Indentation

We believe indent size is like light or dark theme: everyone has their personal preference of what they find more readable, _and it should not impact other contributors_.

To satisfy all sizes, we use tabulations. Each developer is thus free to select their own display size, without impacting anyone else. Code should not depend on the indent size, so vertical alignment is discouraged.

> In GitLab, this is configured in "Preferences → Preferences → Behavior".

> In GitHub, this is configured in "Settings → Appearance → Tab size preference".

> In IntelliJ-based IDEs, this is configured as part of each language's coding style.

If a line is continued in a following line, it should be indented at one more level than the header line.

```javascript
const a = "first line " +
		"second line " +
		"third line";
```

## Spacing

### Whitespace

Put a space between keywords and their parenthesis, but not between functions and theirs.
```c
if (something) {
	execute();
}
```

Do not put a space between delimiters of an argument list.
Put a space after list delimiters, but not before.
```c
execute(5, 1, 2); other(1, 2);
```

Put spaces around binary operators.
Do not put a space between a unary operator and its target, but do put one on the other side.
```c
int a = 2 * 3 + i++ + ++j;
```

### Braces and blocks

When marking the start of a section and the end of a section with characters, the starting character should be at the end of the opening line, each element should be indented on its own line, and the ending character should be unindented on its own line.

```c
if (a == 2) {
	printf("A: %d", a);
}
```

When the closing line has its own keyword, it should be added on the same as the ending character.

```c
if (something) {
	foo();
} else {
	bar();
}
```

```c
do {
	foo()
} while (something);
```

### Long lines

Lines of at most 80 characters are preferred. Lines should not be longer than 120 characters, except to avoid breaking long strings into multiple lines.

When breaking up parameter lists (e.g. a function declarations), make each element in the list its own line:

```kotlin
// OK: it's less than 80 characters
fun foo(a: Int, b: Int, c: Int) = a + b + c

// OK: each element is on its own line
fun foo(
	a: Int,
	b: Int,
	c: Int,
) = a + b + c

// BAD: mix of both
fun foo(
	a: Int, b: Int,
	c: Int,
) = a + b + c
```

When broken on multiple lines and supported by the language, the last element should end with the same separator as used to between the previous elements. For example, in the above examples, elements are separated by commas, so the last element's line should also end with a comma. This helps reduce the size of file diffs and reduce conflicts when multiple patches add elements to the list.

The trailing separator is not used when all elements are on a single line.
