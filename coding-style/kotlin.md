# OpenSavvy coding style for Kotlin

The OpenSavvy coding style for Kotlin is based on the general [OpenSavvy coding style](README.md) and the
official [Kotlin coding style](https://kotlinlang.org/docs/coding-conventions.html).
